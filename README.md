<h1 align="center">GraphQL Fortnite</h1>
<p align="center">
  Get information of a Fortnite Hero with GraphQL!<br />
  <a href="https://graphql-fortnite.now.sh/">See the GraphQL Playground</a>
</p>

## How to use

Simply get Hero's information through queries in GraphQL, example:

```javascript
{
  hero(name: "BASE") {
    name
    class
    characters {
      name
    }
    rarities {
      level
      color
    }
    health
    health_regen
    shield
    shield_regen
    speed
    sprint_speed
    support_squads {
      name
    }
  }
}
```

## Running

```sh
npm install
npm run start
```

## Disclaimer

This was build as a demo project with Apollo Server. 