const { ApolloServer, gql } = require('apollo-server');
const heroes = require('./src/data/heroes');

const typeDefs = gql`

	type Character {
		name: String
	}

	type Rarity {
		level: String
		color: String
	}

	type Squad {
		name: String
	}

	type Hero {
		name: String
		characters: [Character]
		class: String
		rarities: [Rarity]
		health: Int
		health_regen: Int
		shield: Int
		shield_regen: Int
		speed: Int
		sprint_speed: Int 
		support_squads: [Squad]
	}

	type Query {
		heroes(first: Int!, skip: Int): [Hero]
		hero(name: String!): Hero
	}
`

const resolvers = {
	Query: {
		heroes: (parent, {first, skip}, context, info) => {
			const begin = !skip || skip < 0 ? 0 : skip;
			const end = begin + (first > 0 ? first : 0);

			return heroes.slice(begin, end);
		},
		hero: (parent, args, context, info) => {
			return heroes.find(hero => hero.name.toLowerCase() === args.name.toLowerCase())
		}
	},
}

const server = new ApolloServer({
	typeDefs, 
	resolvers,
	introspection: true,
	playground: true
});

server.listen({port: 3000}).then(({url}) => {
	console.log(`Server started at ${url}`);
});
